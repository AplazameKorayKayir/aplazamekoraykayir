import pandas as pd
import numpy as np

from sklearn.base import TransformerMixin

class DataFrameImputer(TransformerMixin):

    def __init__(self):
        """Impute missing values.

        Columns of dtype object are imputed with the most frequent value
        in column.

        Columns of other types are imputed with mean of column.

        """
    def fit(self, X, y=None):

        self.fill = pd.Series([X[c].value_counts().index[0]
            if X[c].dtype == np.dtype('O') else X[c].mean() for c in X],
            index=X.columns)

        return self

    def transform(self, X, y=None):
        return X.fillna(self.fill)

def dataMunging(filePath):

    dataFrame = pd.read_csv(filePath, encoding = "ISO-8859-1")

    print('Shape of data: {}\n'.format(dataFrame.shape))

    threshold = 4956
    nullCount = dataFrame.isnull().sum()
    columnsToDrop = nullCount[nullCount > threshold].index.values

    dataFrame = dataFrame.drop(columnsToDrop, axis=1)

    dataFrame = DataFrameImputer().fit_transform(dataFrame)

    print('Shape of data after dropping columns that has at least 50% null values: {}\n'.format(dataFrame.shape))

    numberOfUnique = dataFrame.apply(pd.Series.nunique)
    columnsToDrop = numberOfUnique[numberOfUnique == 1].index
    noVarianceFeatures = dataFrame[columnsToDrop]
    dataFrame = dataFrame.drop(columnsToDrop, axis=1)

    print('Shape of data after dropping columns that has only 1 unique value: {}\n'.format(dataFrame.shape))

    columnsToDrop = {
            'account/date_joined',
            'account/last_login',
            'confirmed',
            'created',
            'checkout/geoip/properties/ip',
            'checkout/user_agent/value',
            'checkout/user_agent/browser_version'
        }
    dataFrame = dataFrame.drop(columnsToDrop, axis=1)

    print('Shape of data after dropping columns that are noise: {}\n'.format(dataFrame.shape))

    dataFrame = dataFrame[(dataFrame.dtypes == object).index].apply(lambda x: x.astype(str).str.lower())

    print('All string type features converted to lowercase.\n')

    dataFrame.to_csv('dataFrameAfterMunging.csv', index=False)
    noVarianceFeatures.to_csv('noVarianceFeatures.csv', index=False)

    return dataFrame

def dataAnalysis(filePath, filePathNoVariance):
    import seaborn as sns

    dataFrame = pd.read_csv(filePath, encoding = "ISO-8859-1")
    noVarianceFeatures = pd.read_csv(filePathNoVariance)

    sns.distplot(dataFrame['target'], kde=False)
    sns.distplot(dataFrame['account/gender'], kde=False)
    sns.jointplot(x="articles_in_cart/0/discount", y="target", data=dataFrame)

def classification(filePath):

    dataFrame = pd.read_csv(filePath, encoding = "ISO-8859-1")

    # Randomizing order of the rows for random sampling.
    print('Randomizing order of the rows for random sampling.\n')
    dataFrame = dataFrame.sample(frac=1)

    from sklearn import preprocessing

    for f in dataFrame.columns:
        if dataFrame[f].dtype == 'object':
            lbl = preprocessing.LabelEncoder()
            lbl.fit(list(dataFrame[f].values))
            dataFrame[f] = lbl.transform(list(dataFrame[f].values))

    from sklearn.decomposition import PCA

    #n_comp = 43
    #pca = PCA(n_components=n_comp, random_state=420)
    #dataFramePCA = pca.fit_transform(dataFrame.drop(['target'], axis=1))
    #dataFramePCA['target'] = dataFrame['target']
    #dataFrame = dataFramePCA

    trainAndValidationSet = dataFrame[:8000]
    testSet = dataFrame[8001:]

    trainAndValidationTargets = trainAndValidationSet['target']
    testTargets = testSet['target']

    trainAndValidationSet.drop('target', axis = 1, inplace = True)
    testSet.drop('target', axis=1, inplace=True)

    import xgboost as xgb

    from sklearn.model_selection import RandomizedSearchCV
    import scipy.stats as stats

    params = {'n_estimators': stats.randint(150, 500),
              'learning_rate': stats.uniform(0.01, 0.07),
              'subsample': stats.uniform(0.3, 0.7),
              'max_depth': [3, 4, 5, 6, 7, 8, 9],
              'colsample_bytree': stats.uniform(0.5, 0.45),
              'min_child_weight': [1, 2, 3]
            }

    clf = xgb.XGBClassifier(objective = 'binary:logistic')
    nIterSearch = 1250
    randomSearchModel = RandomizedSearchCV(clf, param_distributions=params, n_iter=nIterSearch, scoring='roc_auc',
                                       verbose=10, refit=True)

    randomSearchModel.fit(trainAndValidationSet, trainAndValidationTargets)
    xgb_params = randomSearchModel.best_params_

    print('XGB Parameters are {}', xgb_params)

    dtrain = xgb.DMatrix(trainAndValidationSet, trainAndValidationTargets)
    dtest = xgb.DMatrix(testSet)

    model = xgb.train(dict(xgb_params, silent=1), dtrain, num_boost_round=1250)
    testPrediction = model.predict(dtest)

    from sklearn.metrics import roc_auc_score
    print('Roc prediction score is:\n', roc_auc_score(testTargets, testPrediction))

mungingRequired = True
if (mungingRequired):
    data = dataMunging(filePath = 'data_csv.csv')
    print('Data munging is over.\n')

dataAnalysisRequired = True
if (dataAnalysisRequired):
    dataAnalysis(filePath = 'dataFrameAfterMunging.csv', filePathNoVariance = 'noVarianceFeatures.csv')
    print('Data analysis is over.\n')

classificationRequired = False
if (classificationRequired):
    classification(filePath = 'dataFrameAfterMunging.csv')
    print('Data classification is over.\n')

print('Execution is over.\n')